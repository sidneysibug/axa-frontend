import React from 'react';

const UploadResume = (props) =>{


	const convertBase64 = (file) => {

    return new Promise((resolve,reject)=>{

      const fileReader = new FileReader();
      fileReader.readAsDataURL(file);

      fileReader.onload = ()=>{
        //resolve(fileReader.result)
        resolve(fileReader.result.split(',')[1])

      }

      fileReader.onerror = (error) => {
        reject(error);
      }

    })
  }


  const handleSubmitFile = async (e) => {
    e.preventDefault()


    const inputData = document.querySelector('#resume').files[0];
    //console.log(inputData)

    const fileData = await convertBase64(inputData)
    //console.log(fileData)

    const fileJSON = {
      file: {
      mime: "application/pdf",
      data: fileData
     }
    }

    const proxyurl = "https://cors-anywhere.herokuapp.com/";
    const url = "https://goodmorning-axa-dev.azure-api.net/upload"; 

      fetch(proxyurl + url,{
      method: "post",
      body: JSON.stringify(fileJSON),
      headers: {
        "Content-Type" : "application/json",
        "x-axa-api-key" : "7af58563-a33b-4144-897b-3eb50f13225d"
        
      }
      })
      .then(res => res.json())
      .then(data => {
        if(data){
          console.log(data)
        }
      })

  }

	return (

    <div className="col s12 m4 mx-auto">
      <h5>Upload Resume</h5>

      {/*start of upload*/}
      <input 
        type="file"
        name="Resume"
        id="resume"
      />

      <button 
        type="submit" 
        className="waves-effect waves-light btn"
        onClick={handleSubmitFile}
      >
        Upload
      </button>
      {/*end of upload*/}

          
    </div>

	)
}

export default UploadResume;




