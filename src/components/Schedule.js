import React, {useState} from 'react';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
//import TimePicker from 'react-time-picker';
// import TimePicker from 'react-time-picker/dist/entry.nostyle';
// import Time from 'react-time-format'

const Schedule  = (props) =>{


	const [ proposedDate, setProposedDate] = useState( new Date());

	const [ proposedTime, setProposedTime ] = useState()

	const handleProposedDate = date => {
		setProposedDate(date)
	}

	const ProposedDate = proposedDate.toISOString().split('T')[0];

	//console.log(ProposedDate);

	const handleProposedTime = e => {

		//console.log(e.target.value)
		const inputTime = e.target.value
		//console.log(inputTime)
	
	  var timeSplit = inputTime.split(':'),
	    hours,
	    minutes,
	    meridian;
	  hours = timeSplit[0];
	  minutes = timeSplit[1];
	  if (hours > 12) {
	    meridian = 'PM';
	    hours -= 12;
	  } else if (hours < 12) {
	    meridian = 'AM';
	    if (hours === 0) {
	      hours = 12;
	    }
	  } else {
	    meridian = 'PM';
	  }
	  //console.log(hours + minutes + ' ' + meridian);
	  setProposedTime(hours + minutes + meridian)
		
	}

	//console.log(proposedTime)
	const ProposedTime = proposedTime;
	//console.log(ProposedTime)
	//const online = "true" + ',' ;
	//console.log(JSON.stringify(ProposedDate))
	//console.log(JSON.stringify(ProposedTime))

  const handleSubmit = e => {
  	e.preventDefault()

  	const scheduleData = {
  		ProposedDate: ProposedDate,
  		ProposedTime: ProposedTime,
  		Online: "true",
  	}

  	// const scheduleData = {
  	// 	ProposedDate: "2020-10-09",
  	// 	ProposedTime: "10AM",
  	// 	Online: "true",
  	// }

  	// const scheduleData = '{' + ' "ProposedDate" '+':' + JSON.stringify(ProposedDate) + ',' + ' "ProposedTime" ' + ':' + JSON.stringify(ProposedTime) + ',' + ' "Online" ' + ':' +
  	// ' "true" ' + ',' + '}';

  	//console.log(JSON.stringify(scheduleData))
  	

  	const proxyurl = "https://cors-anywhere.herokuapp.com/";
    const url = "https://goodmorning-axa-dev.azure-api.net/schedule"; 

  	fetch(proxyurl + url,{
      method: "post",
      body: JSON.stringify(scheduleData),
      headers: {
      	Accept: "application/json",
      	'Content-Type' : 'application/json',
     	'x-axa-api-key' : '7af58563-a33b-4144-897b-3eb50f13225d'
      }
      })
      .then(res => res.json())
      .then(data => {
        if(data){
          console.log(data)
        }
      })

  }
	


	return (

		<div className="col s12 m4 mx-auto">
			<h5>Set Schedule</h5>

			{/*start of form*/}
			<form onSubmit={handleSubmit}>

       <label htmlFor="">Proposed Date:</label>
       <DatePicker 
       	selected={proposedDate}
       	onChange={handleProposedDate}

       />

       <label htmlFor="">Set Time</label>
       <input 
       	type="time" 
       	onChange={handleProposedTime}
       />

       <button 
       	type="submit"
       	className="waves-effect waves-light btn"
       	>
       		Schedule
       	</button>
			</form>
			{/*end of form*/}
		</div>

	)
}

export default Schedule;
