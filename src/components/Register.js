import React, { useState} from 'react';

const Register = (props) =>{

  //set state for applicant info
	const [ user, setUser ] = useState({
    Name: "",
    Email: "",
    Mobile: "",
    PositionApplied: "",
    Source: ""
  })

  const [ hasError, setHasError ] = useState(false);

  //get the necessary info from the input value
  const handleChange = e => {
    setUser({
      ...user,
      [ e.target.name ] : e.target.value
    })
  }


  //once clicked, create post method
  const handleSubmit = e => {
    e.preventDefault()
    

    fetch("https://goodmorning-axa-dev.azure-api.net/register",{
      method: "post",
      body: JSON.stringify(user),
      headers: {
        'Content-Type' : 'application/json'
      }
    })
    .then( response => response.json())
    .then( data => data)
    .catch( (error) => {
      
      console.log(error)
      if(error){
        setHasError(true)

      } else {
        setHasError(false)
      }
  
    })
  }



	return (

    <div className="col s12 m4 mx-auto">
          
      {
        hasError ?
        <p className="error">
          Failed to Register
        </p>
        : ""
      }

      <h5>Register</h5>

      {/*start of form*/}
      <form onSubmit={handleSubmit}>
        <input 
          type="text" 
          name="Name" 
          placeholder="Name" 
          handleChange={handleChange}
        />

        <input 
          type="email" 
          name="Email" 
          placeholder="Email" 
          handleChange={handleChange}
        />

        <input 
          type="number" 
          name="Mobile" 
          placeholder="Mobile" 
          handleChange={handleChange}
        />

        <input 
          type="text" 
          name="PositionApplied" 
          placeholder="Position Applied" 
          handleChange={handleChange}
        />

        <input 
          type="text" 
          name="Source" 
          placeholder="Source" 
          handleChange={handleChange}
        />

        <button 
          type="submit" 
          className="waves-effect waves-light btn"
        >
          Register
        </button>


      </form>
      {/*end of form*/}
           
    </div>
	)
}

export default Register;