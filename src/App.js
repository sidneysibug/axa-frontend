import React from 'react';
import Register from './components/Register';
import UploadResume from './components/UploadResume';
import Schedule from './components/Schedule';
import './css/Style.css';




function App() {



  return (

    <div className="container">
    
    	<h2 className="center">AXA Application</h2>
    	<div className="row">

    		<Register />
    		<UploadResume />
    		<Schedule />
    		
    	</div>

    </div>
    
  );
}

export default App;
